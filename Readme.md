# What this is

It's not for the faint of heart. Just kidding. This is a simple Golang implementation of a website that will simply give you a zip file back of a boilerplate api. 

For now, it's just loaded up with a Python3 script that spins out a zip file to the user. 

Simple. Easy Peasy.

## Running

* Docker: `make docker`, will default to 3333 port; override if you like.

* Local: `make run`

## Running with Parms
* Parameters:  You have some choices as to how to parameterize the execution of this (in priority order):
    1. flag: this will come in the form of `--PORT=3333`
    1. envvar: this will come in the form of `PORT=3333`
    2. apistarter.toml: this will come in the form of `PORT=3333`
    3. Defaults to 8888 if not provided.

