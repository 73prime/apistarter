package ui

import (
	"apistarter/api"
	"fmt"
	"html/template"
	"net/http"
)

func GetUIHTML() string {
	html := `
<html>
<head>
<style>
html {
	background-color: #39424e;
}
h1, h2, h3 {
	margin-top: 2em;
	margin-bottom: .5em;
	font-family: didact gothic,sans-serif;
	opacity: .6;
}
body {
  	font-size: 16px;
	font-family: didact gothic,sans-serif;
	color: #fff;
	line-height: 2rem;
	letter-spacing: 1.5px;
	text-shadow: none;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	opacity: 1;
}
* {
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}

</style>
</head>
<body>
<h1>{{.PageTitle}}</h1>
<p>Hey there. Choose one of the items below,
choose a project name, and press the button.</p>
<ul>
{{range .Scripts}}
<li>
<form action="/api">
  <label for="name">{{.Name}}:</label>
  <input type="text" id="name" name="name" value="">
  <input type="hidden" id="apitype" name="apitype" value={{.Name}}>
  <input type="submit" value="Submit"><br/>
  {{.Description}}
</form> 
</li>
{{end}}
</ul>
</body>
</html>
`
	return html
}

type UIData struct {
	PageTitle string
	Scripts   []api.APIBuilderStruct
}
func UIDisplay(w http.ResponseWriter, scripts []api.APIBuilderStruct) {
	//tmpl := template.Must(template.ParseFiles("layout.html"))
	tmpl := template.Must(template.New("myname").Parse(GetUIHTML()))

	fmt.Println(len(scripts))

	data := UIData{
		PageTitle: "Boilerplate API",
		Scripts : scripts,
	}
/*
     {{range $index, $element := .}}{{$index}}
   		<li class="done">{{range $element}}{{.Value}}</li>
       {{end}}
 */
	tmpl.Execute(w, data)
}