package main

import (
	"apistarter/api"
	"github.com/spf13/viper"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)
var testdir string
func setup(){
	viper.Set("port", 5555)
	testdir = "./testscripts"
	os.Mkdir(testdir, 0777)
	cfile, _ := os.Create(testdir + "/tst.sh")
	defer cfile.Close()
	cfile.WriteString("date\n")
	cfile.WriteString("apitype=tester\n")
	cfile.WriteString("project=$1\n")
	cfile.WriteString("folderlocation=$2\n")
	cfile.WriteString("mkdir -p $folderlocation/$project\n")
	cfile.WriteString("mkdir -p $folderlocation/$project/test\n")
	cfile.WriteString("echo blah > $folderlocation/$project/test/blah.txt")
}
func teardown(){
	//nothing here
	os.Remove(testdir + "/tst.sh")
	os.Remove(testdir)
}
func TestMain(m *testing.M){
	setup()
	code := m.Run()
	teardown()
	os.Exit(code)
}

func checkError(err error, t *testing.T) {
	if err != nil {
		t.Errorf("An error occurred: #{err}")
	}
}
func TestInitConfig(t *testing.T) {
	err := initConfig()
	checkError(err, t)
}
func TestGetScriptsReady(t *testing.T) {
	ss, err := api.GetScriptsReady(testdir)
	checkError(err, t)
	if len(ss) != 1{
		t.Errorf("Should have found one script")
	}
	tester := api.GetAPI(ss, "tester")
	if tester == nil {
		t.Errorf("Should have found %s.", "tester")
	}
}
func TestNewAPIHandler(t *testing.T) {
	req, err := http.NewRequest("POST", "", nil)
	checkError(err, t)
	rr := httptest.NewRecorder()
	req.RequestURI = "/"
	http.HandlerFunc(NewAPIHandler).ServeHTTP(rr, req)
	expected := http.StatusBadRequest
	if status := rr.Code; status != expected {
		t.Errorf("Status code differs. Expected %d, got %d", expected, status )
	}

	req, err = http.NewRequest("GET", "", nil)
	checkError(err, t)

	rr = httptest.NewRecorder()
	req.RequestURI = "/"
	http.HandlerFunc(NewAPIHandler).ServeHTTP(rr, req)
	expected = http.StatusBadRequest
	if status := rr.Code; status != expected {
		t.Errorf("Status code differs. Expected %d, got %d", expected, status )
	}
	expectedBody := `{"status":"","error":"Need a 'name' and 'apitype' parameter for this call."}`
	if rr.Body.String() !=  expectedBody{
		t.Errorf("Got %s", rr.Body.String())
	}


	rr = httptest.NewRecorder()
	param := make(url.Values)
	param["apitype"] = []string{"blah"}
	req, err = http.NewRequest("GET", "/?"+param.Encode(), nil)
	checkError(err, t)
	http.HandlerFunc(NewAPIHandler).ServeHTTP(rr, req)
	expected = http.StatusBadRequest
	if status := rr.Code; status != expected {
		t.Errorf("Status code differs. Expected %d, got %d", expected, status )
	}
	expectedBody = `{"status":"","error":"No script found by the type blah"}`
	if rr.Body.String() !=  expectedBody{
		t.Errorf("Got %s", rr.Body.String())
	}

	rr = httptest.NewRecorder()
	param = make(url.Values)
	param["apitype"] = []string{"blah"}
	param["name"] = []string{"blah2"}
	req, err = http.NewRequest("GET", "/?"+param.Encode(), nil)
	checkError(err, t)
	http.HandlerFunc(NewAPIHandler).ServeHTTP(rr, req)
	expected = http.StatusBadRequest
	if status := rr.Code; status != expected {
		t.Errorf("Status code differs. Expected %d, got %d", expected, status )
	}

	APIs, err = api.GetScriptsReady(testdir)
	tester := api.GetAPI(APIs, "tester")
	if tester == nil {
		t.Errorf("Should have found %s.", "tester")
	}
	rr = httptest.NewRecorder()
	param = make(url.Values)
	param["apitype"] = []string{"tester"}
	param["name"] = []string{"blah2"}
	req, err = http.NewRequest("GET", "/?"+param.Encode(), nil)
	checkError(err, t)
	http.HandlerFunc(NewAPIHandler).ServeHTTP(rr, req)
	expected = http.StatusOK
	if status := rr.Code; status != expected {
		t.Errorf("Status code differs. Expected %d, got %d", expected, status )
	}
}
