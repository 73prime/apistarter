.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, or run" with make, por favor.

clean:
	@echo Removing binary
	rm -rf apistarter

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

tests: clean
	@echo Running Tests
	go test --coverprofile=coverage.out ./...

build: clean
	@echo Running build command
	go build -o apistarter main.go

docker:
	docker build -t apistarter:latest . -f Dockerfile
	docker run -it --env PORT=3333 -p 80:3333 apistarter:latest

run: build
	@echo Running program
	./apistarter