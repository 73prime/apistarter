#!/bin/bash
apitype=aws-terraform
description=Builds out a Terraform project suitable to use inside AWS with an S3 remote state.
programname=$1
location=$2

function printUsage() {
    echo "This needs 2 parameters: "
    echo "1: the name of the project"
    echo "2: the folder location of the project"
    echo "NOTE: this will create a project in the location specified, with a folder named after your project"
}
if [[ -z $programname || -z $location ]]; then
    printUsage
    exit 1
fi

newdirectory=$location/$programname
if [ -d "$newdirectory" ]; then
    echo "$newdirectory is a directory, and already exists...exiting."
    exit 1
fi

mkdir -p $newdirectory && cd $newdirectory


# backend.tf
touch backend.tf
cat >> backend.tf << EOF
terraform {
  backend "s3" {
  }
}
EOF

# main.tf
touch main.tf
cat >> main.tf << EOF
#Put your main terraform here....
EOF

# output.tf
touch output.tf
cat >> output.tf << EOF
#Put your output vars here...
#output "primary_rds_endpoint" {
#    value = aws_db_instance.primary.endpoint
#}
EOF
# provider.tf
touch provider.tf
cat >> provider.tf << EOF
provider "aws" {
  region = var.REGION
}
EOF
# terraform.tfvars
touch terraform.tfvars
cat >> terraform.tfvars << EOF
#variable assignments (local, to be put in source control)
#vpc_az = "us-east-1a"
EOF

# variables.tf
touch variables.tf
cat >> variables.tf << EOF
#variable definitions, both envvars, and local.
variable "PROJECT" {}
variable "REGION" {}

#local var here points to terraform.tfvars for setting.
#variable "vpc_az" {}
EOF

# Readme
touch Readme.md
cat >> Readme.md << EOF
# Requirements

* Terraform 0.15.x should work. I recommend installing tfswitch to manage TF versions.
* AWS CLI installed

# Usage

* Refresh AWS login info:
\`\`\`
rm  -rf ~/.aws/credentials
aws configure
\`\`\`
* Create .env file:
\`\`\`
#!/bin/bash
export TF_VAR_REGION="us-east-1"
export TF_VAR_STATE_BUCKET_NAME="tfstate-<your random string>-lamp" 
export TF_VAR_STATE_BUCKET_KEY="tfstate"
export TF_VAR_PROJECT="whatever"
\`\`\`
* Create S3 bucket to hold remote state:
\`\`\`
aws s3api  create-bucket --bucket \$TF_VAR_STATE_BUCKET_NAME --region \$TF_VAR_STATE_REGION
\`\`\`
* Init backend state:
\`\`\`
terraform init --backend-config "bucket=\$TF_VAR_STATE_BUCKET_NAME" --backend-config "key=\$TF_VAR_BUCKET_KEY" --backend-config "region=$TF_VAR_REGION"
\`\`\`
* Terraform plan
\`\`\`
terraform plan
\`\`\`
* Terraform apply
\`\`\`
terraform apply -auto-approve
\`\`\`
* Destroy things:
\`\`\`
terraform destroy -auto-approve
\`\`\`

EOF

cat Readme.md
echo "Terraform project structure written"