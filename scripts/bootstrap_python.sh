#!/bin/bash
apitype=python3
description=Builds out a Python3 API with testsuite, Makefile, and Dockerfile.
project=$1
folderlocation=$2

function help() {
    echo "Please use two parms: "
    echo "First: project name"
    echo "Second: root folder of where you want this to build out."
    echo "This script will build a structure in the root specified."
}
if [[ -z $project || -z $folderlocation ]]; then
    help
    exit 1
fi

newdirectory=$folderlocation/$project
if [ -d "$newdirectory" ]; then
    echo "$newdirectory is a directory, and already exists...exiting."
    exit 1
fi

mkdir -p $newdirectory && cd $newdirectory

mkdir -p tests
touch tests/__init__.py
# test_factors.py
cat >> tests/test_factors.py << EOF
import unittest
from mainfactors import factorCompute

class TestCaseFactor(unittest.TestCase):
    def test_string(self):
        expected = "Not good input"
        actual = factorCompute("a string")
        self.assertEqual(actual, expected)

    def test_negative(self):
        expected = "Not good input"
        actual = factorCompute(-1)
        self.assertEqual(actual, expected)

    def test_6(self):
        expected = '''The factors of 8 are:
1
2
4
8'''
        actual = factorCompute(8)
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
EOF
# mainfactors.py
cat >> mainfactors.py << EOF
def factorCompute(value):
    try:
        val = int(value)
        if val < 1:
            raise Exception('None')
        returnstr = ""
        returnstr = "The factors of {} are:".format(val)
        for i in range(1,val+1):
            if val % i == 0:
                returnstr += "\n" + str(i)
        return returnstr
    except:
        return "Not good input"
EOF

# driver.py
cat >> driver.py << EOF
import os
import socket
from flask import Flask
from mainfactors import factorCompute

app = Flask(__name__)

@app.route("/")
def root():
    svr = socket.gethostname()
    name = "undefined"
    version = "undefined"
    if "name" in os.environ:
        name = os.environ["name"]
    if "version" in os.environ:
        version = os.environ["version"]
    result = "$project loaded up!!"
    result += "<br/>"
    result += "<br>Host named: " + svr
    result += "<br>App named: " + name
    result += "<br>Version is: " + version
    return result

@app.route('/<value>')
def calcfactors(value):
    return factorCompute(value)

if __name__ == '__main__':
    port = int(os.environ.get('PORT',5000))
    app.run(host='0.0.0.0', port=port)
EOF


# requirements.txt
cat >> requirements.txt << EOF
click==7.1.2
Flask==1.1.2
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
Werkzeug==1.0.1
EOF

# .gitignore
cat >> .gitignore << EOF
__pycache__
EOF


# Dockerfile
cat >> Dockerfile << EOF
FROM python:3

RUN mkdir /app
COPY requirements.txt /app
WORKDIR /app
RUN pip install -r requirements.txt

COPY driver.py /app
COPY mainfactors.py /app

CMD [ "python", "./driver.py" ]
EOF

# Makefile
cat >> Makefile << EOF
build: test
	docker build -t $project .

test:
	python -m unittest
EOF

# Readme
cat >> Readme.md << EOF
## First time setup

1. Create a virtual environment: "virtualenv --python=python3 .venv"
2. Activate it: "source .venv/bin/activate"
3. Install Flask: "pip install -r requirements.txt"
4. Now execute your code locally: "python driver.py"
5. Build docker / tests: "make", or "make test"
6. Run docker: "docker run -p 5000:5000 -e name=blah -e version=1.2 $project"
EOF

cd $newdirectory
cat Readme.md
