package api

import (
	"os"
	"testing"

)
var testdir string
func setup(){
	testdir = "./testscripts"
	os.Mkdir(testdir, 0777)
	cfile, _ := os.Create(testdir + "/tst.sh")
	defer cfile.Close()
	cfile.WriteString("date\n")
	cfile.WriteString("apitype=tester\n")
	cfile.WriteString("project=$1\n")
	cfile.WriteString("folderlocation=$2\n")
	cfile.WriteString("mkdir -p $folderlocation/$project\n")
	cfile.WriteString("mkdir -p $folderlocation/$project/test\n")
	cfile.WriteString("echo blah > $folderlocation/$project/test/blah.txt")
}
func teardown(){
	os.Remove(testdir + "/tst.sh")
	os.Remove(testdir)
}
func TestMain(m *testing.M){
	setup()
	code := m.Run()
	teardown()
	os.Exit(code)
}
func checkError(err error, t *testing.T) {
	if err != nil {
		t.Errorf("An error occurred: #{err}")
	}
}
func TestRandomString(t *testing.T) {
	first := RandomString()
	second := RandomString()
	if first == second {
		t.Errorf("Whoops, not so random: %s and %s match.", first, second)
	}
}
func TestGetAPI(t *testing.T) {
	scripts, err := GetScriptsReady(testdir)
	checkError(err, t)
	if len(scripts) != 1{
		t.Errorf("Should have found one script")
	}
	tester := GetAPI(scripts, "tester")
	if tester == nil {
		t.Errorf("Should have found %s.", "tester")
	}
}