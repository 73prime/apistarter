package api

import (
	"archive/zip"
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)
type APIBuilderStruct struct {
	Name string
	Description string
	ScriptExecutable string
}

func GetAPI(apis []APIBuilderStruct, key string) *APIBuilderStruct {
	for _, v := range apis {
		if v.Name == key {
			return &v
		}
	}
	return nil
}
func GetScriptsReady(root string) (apis []APIBuilderStruct, err error){
	//ss = make(map[string]string)
	var files []string
	err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		return nil, err
	}
	for _, file := range files {
		fmt.Println(file)
		f, err := os.Open(file)
		if err != nil {
			fmt.Printf("Error opening file %s....skipping\n", file)
			break
		}
		defer func() {
			if err = f.Close(); err != nil {
				log.Fatal(err)
			}
		}()

		scanner := bufio.NewScanner(f)
		newAPI := APIBuilderStruct{}
		for scanner.Scan() {
			if strings.Contains(scanner.Text(), "apitype=") {
				fullPath, _:= filepath.Abs(file)
				apit := strings.Replace(scanner.Text(),"apitype=","",-1)
				newAPI.Name = apit
				newAPI.ScriptExecutable = fullPath
				fmt.Printf("Found that %s is of type %s\n", fullPath, apit)
			} else if strings.Contains(scanner.Text(), "description=") {
				newAPI.Description = strings.Replace(scanner.Text(), "description=","",-1)
			}
		}
		if newAPI.Name != "" && newAPI.ScriptExecutable != "" {
			apis = append(apis, newAPI)
		}
	}
	return apis, err
}

func RandomString() (randomstring string){
	charSet := "abcdedfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	length := 20
	for i := 0; i < length; i++ {
		random := rand.Intn(len(charSet))
		randomChar := charSet[random]
		randomstring += string(randomChar)
	}
	return randomstring
}
func GenerateProject(name, scriptLocation string, writer http.ResponseWriter) (err error) {
	workingDir := RandomString()
	log.Printf("Creating directory %s", workingDir)
	os.Mkdir("./" + workingDir, 0777)
	/*
		banking on this in each script:
		project=$1
		folderlocation=$2
	*/
	cmd := exec.Command("/bin/sh", scriptLocation, name, "./" + workingDir)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		log.Printf("Error execing: %s", stderr.String())
		log.Printf("An error execing script, removing directory %s now", workingDir)
		RemoveContents(workingDir)
		return err
	}
	log.Printf("Exec'd the script %s without error.", scriptLocation)
	//fmt.Println(out.String())
	//zip files now.
	zipname := workingDir + "_" + name + ".zip"
	err = ZipWriter(workingDir + "/", zipname)
	if err != nil {
		return err
	}
	Openfile, err := os.Open(zipname)
	defer Openfile.Close()
	if err != nil {
		return err
	}
	log.Printf("Created zip file %s, now packaging up for the http response.", zipname)
	//gets content-type of file
	FileHeader := make([]byte, 512)
	Openfile.Read(FileHeader)
	FileContentType := http.DetectContentType(FileHeader)
	//size now
	FileStat, _ := Openfile.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)
	writer.Header().Set("Content-Disposition", "attachment; filename=" + zipname)
	writer.Header().Set("Content-Type", FileContentType)
	writer.Header().Set("Content-Length", FileSize)
	//we read 512 bytes, so need to shift back.
	Openfile.Seek(0,0)
	io.Copy(writer, Openfile)

	err = RemoveContents(workingDir)
	if err != nil {
		log.Printf("Removed directory %s", workingDir)
	} else {
		err = os.Remove(zipname)
		if err != nil {
			log.Printf("Removed zip file %s", zipname)
		}
	}
	return err
}

func RemoveContents(dir string) (err error) {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	os.Remove(dir)
	return err
}
func ZipWriter(baseFolder, outputZip string) (err error){
	// Get a Buffer to Write To
	outFile, err := os.Create(outputZip)
	if err != nil {
		return err
	}
	defer outFile.Close()

	// Create a new zip archive.
	w := zip.NewWriter(outFile)

	// Add some files to the archive.
	addFiles(w, baseFolder, "")

	// Make sure to check the error on Close.
	err = w.Close()
	return err
}
func addFiles(w *zip.Writer, basePath, baseInZip string) {
	// Open the Directory
	files, err := ioutil.ReadDir(basePath)
	if err != nil {
		fmt.Println(err)
	}

	for _, file := range files {
		fmt.Println(basePath + file.Name())
		if !file.IsDir() {
			dat, err := ioutil.ReadFile(basePath + file.Name())
			if err != nil {
				fmt.Println(err)
			}

			// Add some files to the archive.
			f, err := w.Create(baseInZip + file.Name())
			if err != nil {
				fmt.Println(err)
			}
			_, err = f.Write(dat)
			if err != nil {
				fmt.Println(err)
			}
		} else if file.IsDir() {

			// Recurse
			newBase := basePath + file.Name() + "/"
			fmt.Println("Recursing and Adding SubDir: " + file.Name())
			fmt.Println("Recursing and Adding SubDir: " + newBase)

			addFiles(w, newBase, baseInZip  + file.Name() + "/")
		}
	}
}
