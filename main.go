package main

import (
	"apistarter/api"
	"apistarter/ui"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"io"
	"log"
	"math/rand"
	"net/http"
	"time"
)
//var scripts map[string]string
var APIs []api.APIBuilderStruct

func EmbedHeaders(w http.ResponseWriter){
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Add("Content-Type", "application/json")
}
func UIHandler(w http.ResponseWriter, r *http.Request){
	ui.UIDisplay(w, APIs)
}
func NewAPIHandler(w http.ResponseWriter, r *http.Request) {
	type retstruct struct {
		Status string `json:"status"`
		Error string `json:"error"`
	}
	var retobj retstruct
	EmbedHeaders(w)
	switch r.Method{
	case http.MethodGet:
		fmt.Println("GET params were:", r.URL.Query())
		apitype := r.URL.Query().Get("apitype")
		if len(apitype) > 0 {
			//if scripts[apitype] == "" {
			foundAPI := api.GetAPI(APIs, apitype)
			if foundAPI == nil {
				w.WriteHeader(http.StatusBadRequest)
				retobj.Error = fmt.Sprintf("No script found by the type %s", apitype)
			} else {
				projname := r.URL.Query().Get("name")
				if len(projname) > 0 {
					err := api.GenerateProject(projname, foundAPI.ScriptExecutable, w)
					if err != nil {
						w.WriteHeader(http.StatusInternalServerError)
						retobj.Error = err.Error()
					} else {
						w.WriteHeader(http.StatusOK)
						retobj.Status = "Good"
					}
				} else {
					w.WriteHeader(http.StatusBadRequest)
					retobj.Error = "Need a 'name' parameter."
				}
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			retobj.Error = "Need a 'name' and 'apitype' parameter for this call."
		}
		js, _ := json.Marshal(retobj)
		log.Printf("Sending %s back to caller.\n", string(js))
		io.WriteString(w, string(js))
	default:
		retobj.Status = "Bad"
		retobj.Error = "Bad, very bad."
		w.WriteHeader(http.StatusBadRequest)
		js, _ := json.Marshal(retobj)
		log.Printf("Sending %s back to caller.\n", string(js))
		io.WriteString(w, string(js))
	}
}

func handleRequests() {
	http.HandleFunc("/api", NewAPIHandler)
	http.HandleFunc("/ui", UIHandler)
	fmt.Printf("Started Webserver on port %d.\n", viper.GetInt("port"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d",viper.GetInt("port")), nil))
}
func initConfig() error {

	fmt.Println("Starting apistarter....")
	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		return err
	}
	viper.AddConfigPath(home)
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("~")
	viper.SetConfigName("apistarter.toml")
	pflag.Int("PORT", 8888, "This is the port to bind to. 8888 is default.")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println("Uh oh. no config file")
		err = nil
	}
	fmt.Printf("Will use %d as port number.\n", viper.GetInt("port"))
	if viper.GetInt("port") == 0 {
		err = errors.New("No valid port number")
	}
	return err
}

func main() {
	rand.Seed(time.Now().Unix())
	err := initConfig()
	APIs, err = api.GetScriptsReady("./scripts")
	if err == nil {
		handleRequests()
	} else {
		log.Fatalln(err.Error())
	}
}
